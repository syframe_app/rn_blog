'use strict';

import * as types from '../constants/ActionTypes';

const initialState = {
    loading: false,
    typeList: {}
}

export default function category(state = initialState, action) {
    // 上面方式:一个技巧是使用 ES6 参数默认值语法 来精简代码。
    // if (typeof state === 'undefined') {
    //     return initialState
    // }
    switch (action.type) {
        case types.FETCH_TYPE_LIST:
            return Object.assign({}, state, {
                loading: true
            });
        case types.RECEIVE_TYPE_LIST:
            return Object.assign({}, state, {
                loading: false,
                typeList: action.typeList
            })
        default:
            return state;
    }
}
