'use strict';

import React, {PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid
} from 'react-native';

import {NaviGoBack} from '../utils/CommonUtils';

/**
 * 类似于定义接口
 */
const propTypes = {
    title: PropTypes.string,
    actions: PropTypes.array,
    navigator: PropTypes.object,
    onActionSelected: PropTypes.func,
    onIconClicked: PropTypes.func,
    navIcon: PropTypes.number,
    customView: PropTypes.object
};

/**
 * 封装的Toolbar组件
 */
class ReadingToolbar extends React.Component {

    constructor(props) {
        super(props);
        this.onIconClicked = this.onIconClicked.bind(this);
        this.onActionSelected = this.onActionSelected.bind(this);
    }

    onIconClicked() {
        if (this.props.onIconClicked) {
            this.props.onIconClicked();
        } else {
            const {navigator} = this.props;
            if (navigator) {
                NaviGoBack(navigator);
            }
        }
    }

    onActionSelected(position) {
        this.props.onActionSelected();
    }

    render() {
        if (this.props.customView) {//一般情况下,调用系统自带的,不做处理
            return (
                <ToolbarAndroid style={styles.toolbar}>
                    {this.props.customView}
                </ToolbarAndroid>
            )
        } else {
            return (
                <ToolbarAndroid
                    style={styles.toolbar}
                    actions={this.props.actions}
                    onActionSelected={this.onActionSelected}//当一个功能被选中的时候调用此回调。传递给此回调的唯一参数是该功能在actions数组中的位置。
                    onIconClicked={this.onIconClicked}//当图标被选中的时候调用此回调。
                    navIcon={this.props.navIcon ? this.props.navIcon : require('../../assets/imgs/icon_left.png')}//设置导航器的icon。
                    titleColor='#fff'//设置工具栏的标题颜色。
                    title={this.props.title}//设置工具栏的标题。
                />
            );
        }
    }
}

let styles = StyleSheet.create({
    toolbar: {
        backgroundColor: '#3e9ce9',
        height: 58
    }
});

ReadingToolbar.propTypes = propTypes;

/**
 * 自定义属性
 */
ReadingToolbar.defaultProps = {
    onActionSelected: function () {

    },
    title: '',
    actions: []
};

export default ReadingToolbar;