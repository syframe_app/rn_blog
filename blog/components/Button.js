'use strict';

import React, {PropTypes} from 'react';
import {
    Text,
    TouchableOpacity
} from 'react-native';

import StyleSheetPropType from 'StyleSheetPropType';
import ViewStylePropTypes from 'ViewStylePropTypes';
import TextStylePropTypes from 'TextStylePropTypes';

let ViewStylePropType = StyleSheetPropType(ViewStylePropTypes);
let TextStylePropType = StyleSheetPropType(TextStylePropTypes);

const propTypes = {
    onPress: PropTypes.func,
    disabled: PropTypes.bool,
    style: TextStylePropType,
    containerStyle: ViewStylePropType,
    text: PropTypes.string
};

/**
 * 自定义封装常用的BTN
 */
class Button extends React.Component {
    constructor(props) {
        super(props);
        this.onPress = this.onPress.bind(this);
    }

    onPress() {
        if (this.props.disabled) {
            return;
        }
        this.props.onPress();
    }

    render() {
        return (
            <TouchableOpacity
                style={this.props.containerStyle}
                onPress={this.onPress}>
                <Text style={this.props.style}>
                    {this.props.text}
                </Text>
            </TouchableOpacity>
        );
    }
}

Button.propTypes = propTypes;

/**
 * 接口,自定义回调
 * @type {{onPress: Button.defaultProps.onPress, disabled: boolean}}
 */
Button.defaultProps = {
    onPress: function () {
    },
    disabled: false
};

export default Button;