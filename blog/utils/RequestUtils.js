'use strict';

let HOST = 'http://apis.baidu.com/';

/**
 * 封装网络请求工具类
 * @param url
 * @param method
 * @param body
 * @returns {Promise}
 */
export function request(url, method, body) {
  var isOk;
  return new Promise((resolve, reject) => {
    fetch(HOST + url, {
        method: method,
        headers: {
          'apikey': 'c33d9be07601bd1f27ce42f0d32326bd'
        },
        body: body,
      })
      .then((response) => {
        if (response.ok) {
          isOk = true;
        } else {
          isOk = false;
        }
        return response.json();
      })
      .then((responseData) => {
        if (isOk) {
          resolve(responseData);
        } else {
          reject(responseData);
        }
      })
      .catch((error) => {
        reject(error);
      });
  })
}