'use strict';

import * as types from '../constants/ActionTypes';
import {ToastShort} from '../utils/ToastUtils';
import {request} from '../utils/RequestUtils';
import {WEXIN_ARTICLE_LIST} from '../constants/Urls';

/**
 * 网络请求,数据类似于model层
 * @param isRefreshing
 * @param loading
 * @param typeId
 * @param isLoadMore
 * @param page
 * @returns {function()}
 */
export function fetchArticles(isRefreshing, loading, typeId, isLoadMore, page) {
	if (page == undefined) {
		page = 1;
	}

	return dispatch => {
		dispatch(fetchArticleList(isRefreshing, loading, isLoadMore));
		return request(WEXIN_ARTICLE_LIST + '?typeId=' + typeId + '&page=' + page, 'get')
      .then((articleList) => {
        dispatch(receiveArticleList(articleList.showapi_res_body.pagebean.contentlist, typeId));
      })
      .catch((error) => {
        dispatch(receiveArticleList([], typeId));
        ToastShort(error.message);
      })
	}
}

/**
 *Action 本质上是 JavaScript 普通对象。我们约定，action 内必须使用一个字符串类型的 type 字段来表示将要执行的动作
 *
 *应该尽量减少在 action 中传递的数据。比如上面的例子，传递 index 就比把整个任务对象传过去要好。
 *
 * Action 创建函数 就是生成 action 的方法
 *在 Redux 中的 action 创建函数只是简单的返回一个 action
 * 这样做将使 action 创建函数更容易被移植和测试
 */
function fetchArticleList(isRefreshing, loading, isLoadMore) {
	if (isLoadMore == undefined) {
		isLoadMore = false;
	}
	return {
		type: types.FETCH_ARTICLE_LIST,
		isRefreshing: isRefreshing,
		loading: loading,
		isLoadMore: isLoadMore
	}
}

function receiveArticleList(articleList, typeId) {
	return {
		type: types.RECEIVE_ARTICLE_LIST,
		articleList: articleList,
		typeId: typeId
	}
}