'use strict';

import React from 'react';
import {
    StyleSheet,
    WebView,
    BackAndroid,
    Dimensions,
    Text,
    Image,
    Platform,
    TouchableOpacity,
    View
} from 'react-native';

import {shareToTimeline, shareToSession} from 'react-native-wechat';

// import Portal from 'react-native/Libraries/Portal/Portal.js';

import ReadingToolbar from '../../components/ReadingToolbar';
import {ToastShort} from '../../utils/ToastUtils';
import LoadingView from '../../components/LoadingView';
import {NaviGoBack} from '../../utils/CommonUtils';

let tag;
let toolbarActions = [
    {title: '分享', icon: require('../../../assets/imgs/share.png'), show: 'always'}
];

var canGoBack = false;

class WebViewPage extends React.Component {
    constructor(props) {
        super(props);
        this.onActionSelected = this.onActionSelected.bind(this);
        this.onNavigationStateChange = this.onNavigationStateChange.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    componentWillMount() {
        // if (Platform.OS === 'android') {
        //     tag = Portal.allocateTag();
        // }
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.goBack);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.goBack);
    }

    onActionSelected() {
        //分享对话框
        // Portal.showModal(tag, this.renderSpinner());
    }

    onNavigationStateChange(navState) {
        canGoBack = navState.canGoBack;
    }

    goBack() {
        // if (Portal.getOpenModals().length !== 0) {
        //     Portal.closeModal(tag);
        //     return true;
        // } else if (canGoBack) {
        //     this.refs.webview.goBack();
        //     return true;
        // }
        return NaviGoBack(this.props.navigator);
    }

    renderLoading() {
        return <LoadingView />;
    }

    /**
     * 分享对话框
     * @returns {XML}
     */
    renderSpinner() {
        const {route} = this.props;
        return (
            <View
                key={'spinner'}
                style={styles.spinner}
              >
                <View style={styles.spinnerContent}>
                    <Text style={[styles.spinnerTitle, {fontSize: 20, color: 'black'}]}>
                        分享到
                    </Text>
                    <View style={{flexDirection: 'row', marginTop: 20}}>
                        <TouchableOpacity //微信
                            style={{flex: 1}}
                            onPress={() => {
                                shareToSession({
                                  title: route.article.title,
                                  description: '分享自：Blog',
                                  thumbImage: route.article.contentImg,
                                  type: 'news',
                                  webpageUrl: route.article.url
                                })
                                .catch((error) => {
                                  ToastShort(error.message);
                                });
                          }}>
                            <View style={styles.shareContent}>
                                <Image
                                    style={styles.shareIcon}
                                    source={require('../../../assets/imgs/share_icon_wechat.png')}
                                />
                                <Text style={styles.spinnerTitle}>
                                    微信
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity //朋友圈
                                style={{flex: 1}}
                                onPress={() => {
                                shareToTimeline({
                                  title: '[@Reading]' + route.article.title,
                                  thumbImage: route.article.contentImg,
                                  type: 'news',
                                  webpageUrl: route.article.url
                                })
                                .catch((error) => {
                                  ToastShort(error.message);
                                });
                            }}>
                            <View style={styles.shareContent}>
                                <Image
                                    style={styles.shareIcon}
                                    source={require('../../../assets/imgs/share_icon_moments.png')}
                                />
                                <Text style={styles.spinnerTitle}>
                                    朋友圈
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    render() {
        const {navigator, route} = this.props;
        return (
            <View style={styles.container}>
                <ReadingToolbar
                    actions={toolbarActions}
                    onActionSelected={this.onActionSelected} //当一个功能被选中的时候调用此回调。传递给此回调的唯一参数是该功能在actions数组中的位置。
                    title={route.article.userName}
                    navigator={navigator}
                />
                <WebView
                    ref='webview'
                    automaticallyAdjustContentInsets={false}
                    style={{flex: 1}}
                    source={{uri: route.article.url}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    startInLoadingState={true}
                    scalesPageToFit={true}
                    decelerationRate="normal"
                    onShouldStartLoadWithRequest={true}
                    onNavigationStateChange={this.onNavigationStateChange}
                    renderLoading={this.renderLoading.bind(this)}//设置一个函数，返回一个加载指示器。
                />
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    spinner: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.65)'
    },
    spinnerContent: {
        justifyContent: 'center',
        width: Dimensions.get('window').width * (7 / 10),
        height: Dimensions.get('window').width * (7 / 10) * 0.68,
        backgroundColor: '#fcfcfc',
        padding: 20,
        borderRadius: 5
    },
    spinnerTitle: {
        fontSize: 18,
        color: '#313131',
        textAlign: 'center'
    },
    shareContent: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    shareIcon: {
        width: 40,
        height: 40
    }
});

export default WebViewPage;