/**
 * Action 是把数据从应用（这里之所以不叫 view 是因为这些数据有可能是服务器响应，用户输入或其它非 view 的数据 ）传到 store 的有效载荷。
 * 它是 store 数据的唯一来源。
 * 一般来说你会通过 store.dispatch() 将 action 传到 store。
 * Action 本质上是 JavaScript 普通对象。我们约定，action 内必须使用一个字符串类型的 type 字段来表示将要执行的动作
 * 除了 type 字段外，action 对象的结构完全由你自己决定。参照 Flux 标准 Action 获取关于如何构造 action 的建议。
 *
 *
 * 与store的数据交互
 * 在 传统的 Flux 实现中，当调用 action 创建函数时，一般会触发一个 dispatch;
 * 不同的是，Redux 中只需把 action 创建函数的结果传给 dispatch() 方法即可发起一次 dispatch 过程;如:dispatch(addTodo(text))
 * 或者创建一个 被绑定的 action 创建函数 来自动 dispatch;如:const boundAddTodo = (text) => dispatch(addTodo(text))  boundAddTodo(text);
 *
 * store 里能直接通过 store.dispatch() 调用 dispatch() 方法，但是多数情况下你会使用 react-redux 提供的 connect() 帮助器来调用。
 * bindActionCreators() 可以自动把多个 action 创建函数 绑定到 dispatch() 方法上。
 *
 */