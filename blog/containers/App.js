'use strict';

import React from 'react';
import {
    StyleSheet,
    Navigator,
    StatusBar,
    BackAndroid,
    View
} from 'react-native';

import Splash from '../pages/Splash';
import {registerApp} from 'react-native-wechat';
import {NaviGoBack} from '../utils/CommonUtils';

var _navigator, isRemoved = false;

class App extends React.Component {

    constructor(props) {
        super(props);
        //微信分享
        registerApp('wxfc1ea0c305c1a0a4');
        this.renderScene = this.renderScene.bind(this);
        this.goBack = this.goBack.bind(this);

        BackAndroid.addEventListener('hardwareBackPress', this.goBack);
    }

    goBack() {
        return NaviGoBack(_navigator);
    }

    /**
     * 后期也可以做成公共的回退
     * @returns {XML}
     */
    renderScene(route, navigator) {
        let Component = route.component;
        _navigator = navigator;

        if (route.name === 'WebViewPage') {
            BackAndroid.removeEventListener('hardwareBackPress', this.goBack);
            isRemoved = true;
        } else {
            if (isRemoved) {
                BackAndroid.addEventListener('hardwareBackPress', this.goBack);
            }
        }

       /* return (<Component navigator={navigator} route={route}/> );*/
        return <Component {...route.params} navigator={navigator} />
    }

    configureScene(route, routeStack) {
        return Navigator.SceneConfigs.PushFromRight;
    }

    /**
     * 后期也可以做成公共的头部
     * @returns {XML}
     */
    render() {
        let name = 'Splash';
        let splash = Splash;

        return (
            <View style={{flex: 1}}>
                <StatusBar //StatusBar组件可以同时加载多个。此时属性会按照加载顺序合并（后者覆盖前者）。一个典型的用法就是在使用Navigator时，针对不同的路由指定不同的状态栏样式，如下：
                    backgroundColor="#3e9ce9"
                    barStyle="default"//设置状态栏文本的颜色
                />
                <Navigator
                    ref='navigator'
                    style={styles.navigator}
                    /*有两种方式*/
                    /*configureScene={this.configureScene}*/                    
                    configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
                    /*initialRoute={{
                        component: Splash,
                        name: 'Splash'
                     }}*/
                    initialRoute={{ name: name, component: splash }}
                    /*renderScene={this.renderScene}*/
                    renderScene={(route, navigator) => {
                       let Component = route.component;
                       return <Component {...route.params} navigator={navigator} />
                    }} 
                />
            </View>
        );
    }
}

let styles = StyleSheet.create({
    navigator: {
        flex: 1
    }
});

export default App;