// if (process.env.NODE_ENV === 'production') {
//   module.exports = require('./configureStore.prod');
// } else {
//   module.exports = require('./configureStore.dev');
// }

'use strict';

import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from '../reducer'

import { persistState } from 'redux-devtools'
import DevTools from '../containers/dev/DevTools'

// import api from '../middleware/api'
/**
 *
 * Store 就是把它们联系到一起的对象。Store 有以下职责：
   维持应用的 state；
   提供 getState() 方法获取 state；
   提供 dispatch(action) 方法更新 state；
   通过 subscribe(listener) 注册监听器;
   通过 subscribe(listener) 返回的函数注销监听器。

 Redux store 保存了根 reducer 返回的完整 state 树。
  这个新的树就是应用的下一个 state！所有订阅 store.subscribe(listener) 的监听器都将被调用；监听器里可以调用 store.getState() 获得当前 state。
  现在，可以应用新的 state 来更新 UI。如果你使用了 React Redux 这类的绑定库，这时就应该调用 component.setState(newState) 来更新。
 */

/**
 * 在应用中，只有最顶层组件是对 Redux 可知（例如路由处理）这是很好的。所有它们的子组件都应该是“笨拙”的，并且是通过 props 获取数据。
 * 用 react-redux 提供的 connect() 方法将“笨拙”的 Counter 转化成容器组件。
 * connect() 允许你从 Redux store 中指定准确的 state 到你想要获取的组件中。这让你能获取到任何级别颗粒度的数据。
 * @param initialState
 * @returns {*}
 */

// const enhancer = compose(
//     DevTools.instrument(),
//     persistState(
//         window.location.href.match(
//             /[?&]debug_session=([^&#]+)\b/
//         )
//     )
// );

/**异步数据流
 * 像 redux-thunk 或 redux-promise 这样支持异步的 middleware 都包装了 store 的 dispatch() 方法，
 * 以此来让你 dispatch 一些除了 action 以外的其他内容，例如：函数或者 Promise。
 * 
 * 通过使用指定的 middleware，action creator 除了返回 action 对象外还可以返回函数。这时，这个 action creator 就成为了 thunk。
 */
//使用 Thunk Middleware 来做异步 Action
const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);

//compose(...functions)
// 这是函数式编程中的方法，为了方便，被放到了 Redux 里。 当需要把多个 store 增强器 依次执行的时候，需要用到它。
export default function configureStore(initialState) {
  const store = createStoreWithMiddleware(rootReducer, initialState);
//thunkMiddleware 允许我们 dispatch() 函数
  // const store = createStore(rootReducer, initialState, enhancer);
/*  const store = createStore(
      rootReducer,
      initialState,
      // createLogger()
      compose(
          applyMiddleware(thunkMiddleware,createLogger()),
          DevTools.instrument()
      )
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    })
  }*/

  return store;
}

// 发起 Actions
/**
 * import { addTodo, completeTodo, setVisibilityFilter, VisibilityFilters } from './actions'

 // 打印初始状态
 console.log(store.getState())

 // 每次 state 更新时，打印日志
 // 注意 subscribe() 返回一个函数用来注销监听器
 let unsubscribe = store.subscribe(() =>
 console.log(store.getState())
 )

 // 发起一系列 action
 store.dispatch(addTodo('Learn about actions'))
 store.dispatch(addTodo('Learn about reducers'))
 // 停止监听 state 更新
 unsubscribe();
 *
 */
