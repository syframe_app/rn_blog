'use strict';

/**
 * 导航结束,返回公交类,不错推荐用这样的;
 * @param navigator
 * @returns {boolean}
 * @constructor
 */
export function NaviGoBack(navigator) {
	if (navigator && navigator.getCurrentRoutes().length > 1) {
		navigator.pop();
		return true;
  }
  return false;
}

export function isEmptyObject(obj) {
	for (var name in obj) {
		return false;
	}
	return true;
}