'use strict';

import React from 'react';
import Root from './blog/Root';

import {AppRegistry} from 'react-native';
AppRegistry.registerComponent('app-blog', () => Root);
