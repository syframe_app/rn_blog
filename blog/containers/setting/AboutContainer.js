'use strict';

import React from 'react'
import {connect} from 'react-redux'

import About from '../../pages/setting/About'

/**
 * 关于界面的About容器界面;用react-redux方便了很好的管理(不用自己去维护navigator)
 */
class AboutContainer extends React.Component {
  render() {
    return (
      <About {...this.props} />
    )
  }
}

// function mapStateToProps(state) {
//   const {read} = state;
//   return {
//     read
//   };
// }
//
// export default connect(mapStateToProps)(AboutContainer);
export default AboutContainer