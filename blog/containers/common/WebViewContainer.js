'use strict';

import React from 'react';
import {connect} from 'react-redux';

import WebViewPage from '../../pages/common/WebViewPage';

/**
 * WebView容器界面;用react-redux方便了很好的管理(不用自己去维护navigator)
 */
class WebViewContainer extends React.Component {
  render() {
    return (
      <WebViewPage {...this.props} />
    );
  }
}

function mapStateToProps(state) {
  const {read} = state;
  return {
    read
  };
}

export default connect(mapStateToProps)(WebViewContainer);
