'use strict';

import {combineReducers} from 'redux';
import read from './read';
import category from  './category';
import welfare from  './welfare';

/**
 * 之所以称作 reducer 是因为它将被传递给 Array.prototype.reduce(reducer, ?initialValue) 方法
 * reducer 就是一个纯函数，接收旧的 state 和 action，返回新的 state。格式:(previousState, action) => newState
 * 主要还是更新state;
 * 在 Redux 应用中，所有的 state 都被保存在一个单一对象中;建议在写代码前先想一下这个对象的结构;
 * 根 reducer 应该把多个子 reducer 输出合并成一个单一的 state 树。
 * 尽量把这些数据与 UI 相关的 state 分开;
 * 记住 reducer 只是函数而已，所以你可以尽情使用函数组合和高阶函数这些特性。
 *
 * 保持 reducer 纯净非常重要。永远不要在 reducer 里做这些操作：
     1:修改传入参数；
     2:执行有副作用的操作，如 API 请求和路由跳转；
     3:调用非纯函数，如 Date.now() 或 Math.random()。
 只要传入参数相同，返回计算得到的下一个 state 就一定相同。没有特殊情况、没有副作用，没有 API 请求、没有变量修改，单纯执行计算。

 需要做的只是改变 state 中的 visibilityFilter:
 注意:
 1:不要修改 state。 使用 Object.assign() 新建了一个副本。不能这样使用 Object.assign(state, { visibilityFilter: action.filter })，因为它会改变第一个参数的值。
    你必须把第一个参数设置为空对象。你也可以开启对ES7提案对象展开运算符的支持, 从而使用 { ...state, ...newState } 达到相同的目的。
 2:在 default 情况下返回旧的 state。遇到未知的 action 时，一定要返回旧的 state。
 ps:Object.assign() 是 ES6 特性，但多数浏览器并不支持。你要么使用 polyfill，Babel 插件，或者使用其它库如 _.assign() 提供的帮助方法。

 */
//Redux 提供了 combineReducers() 工具类来做上面 todoApp 做的事情，这样就能消灭一些样板代码了
//每个 reducer 根据它们的 key 来筛选出 state 中的一部分数据并处理，然后这个生成的函数再将所有 reducer 的结果合并成一个大的对象。
// combineReducers 接收一个对象，可以把所有顶级的 reducer 放到一个独立的文件中，通过 export 暴露出每个 reducer 函数，
// 然后使用 import * as reducers 得到一个以它们名字作为 key 的 object：如:import * as reducers from './reducers'  const todoApp = combineReducers(reducers)

/*const rootReducer = combineReducers({
    //注意每个 reducer 只负责管理全局 state 中它负责的一部分。每个 reducer 的 state 参数都不同，分别对应它管理的那部分 state 数据。
    read,
    category
})

export default rootReducer;*/


 // 注意上面的写法和下面完全等价
//可以给它们设置不同的 key，或者调用不同的函数
export default function rootReducer(state = {}, action) {
    return {
        read: read(state.read, action),
        category: category(state.category, action),
        welfare: category(state.welfare, action)
    }
}
export default rootReducer;
