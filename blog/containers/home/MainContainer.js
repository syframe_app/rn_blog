'use strict';

import React from 'react'
import {connect} from 'react-redux'
import CodePush from 'react-native-code-push'
// import AV from 'avoscloud-sdk'
import Main from '../../pages/home/Main'
import Storage from '../../utils/Storage'

let typeIds = [0, 12, 9, 2]

/**
 * 初始化,第三方必备数据
 *
 * Redux 的 React 绑定库包含了 容器组件和展示组件相分离 的开发思想。
 *明智的做法是只在最顶层组件（如路由操作）里使用 Redux。其余内部组件仅仅是展示性的，所有数据都通过 props 传入。
 */
class MainContainer extends React.Component {

    componentDidMount() {
        //版本更新
        // CodePush.sync({
        //     //更MainActivityDebug中注册的对应上；
        //     deploymentKey: "_rDo8hy8sWhX6dKhr2mRagOKAd2wNkZzYvxBW",
        //     // updateDialog: true,
        //     updateDialog: {
        //         optionalIgnoreButtonLabel: '稍后',
        //         optionalInstallButtonLabel: '后台更新',
        //         optionalUpdateMessage: 'Blog有新版本了，是否更新？',
        //         title: '更新提示'
        //     },
        //     installMode: CodePush.InstallMode.ON_NEXT_RESTART
        // })

        //反馈,推送
        // AV.initialize('Tfi1z7dN9sjMwSul8sYaTEvg-gzGzoHsz', '57qmeEJonefntNqRe17dAgi4')

        Storage.get('isInit')
            .then((isInit) => {
                if (!isInit) {
                    Storage.save('typeIds', typeIds)
                    Storage.save('isInit', true)
                }
            })
    }

    render() {
        return (
            <Main {...this.props} />
        );
    }
}
// 哪些 Redux 全局的 state 是我们组件想要通过 props 获取的？
function mapStateToProps(state) {
    const {read} = state;
    return {
        read
    }
}

// 哪些 action 创建函数是我们想要通过 props 获取的？
/*function mapDispatchToProps(dispatch) {
    return {
        onIncrement: () => dispatch(increment())
    };
}*/

//在调用 connect() 方法的时候使用了两个括号。这个叫作局部调用
export default connect(mapStateToProps)(MainContainer)
/**
export default connect(
    state => ({ todos: state.todos })
)(MainContainer)
 */
