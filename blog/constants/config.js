var config = {
    'userAgent': 'sample-rn',
}

/**
 * 常量和key值配置
 */
const constant = {
    code_push: {
        STAGING_KEY: "_rDo8hy8sWhX6dKhr2mRagOKAd2wNkZzYvxBW",
        PRODUCTION_KEY: "IlCGQBVRA9WtomZ_7JdwSvW6fWs6NkZzYvxBW",
    },
    //APP总体路由配置
    scene: {
        project: { key: "project", value: "项目" },
        famous: { key: "famous", value: "发现" },
        personal: { key: "personal", value: "Me" },

        login: { key: "login", value: "登陆" },
        search: { key: "search", value: "搜索项目" },
        create_issue: { key: "create_issue", value: "创建Issue" },
        shake: { key: "shake", value: "摇一摇" },
        feedback: { key: "feedback", value: "意见反馈" },
        settings: { key: "settings", value: "设置" },
        my_profile: { key: "my_profile", value: "我的资料" },
        repo_detail: { key: "repo_detail", value: "项目详情" },
        web: { key: "web", value: "web" },
    }
}
module.exports = config;
module.exports.constant = constant;
