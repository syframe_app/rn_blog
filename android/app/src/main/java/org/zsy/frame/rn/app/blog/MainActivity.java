package org.zsy.frame.rn.app.blog;

import android.support.annotation.Nullable;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.microsoft.codepush.react.CodePush;
import com.theweflex.react.WeChatPackage;
import java.util.Arrays;
import java.util.List;

//public class MainActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.act_main);
//    }
//}

public class MainActivity extends ReactActivity {

  @Override protected String getMainComponentName() {
    return BuildConfig.APP_NAME;
  }

  /**CodePush*/
  //private CodePush codePush;

  private String getKey(){
    String deploymentKey;
    //if (BuildConfig.DEBUG) {
    deploymentKey = "_rDo8hy8sWhX6dKhr2mRagOKAd2wNkZzYvxBW";
    //} else {
    //  deploymentKey = "IlCGQBVRA9WtomZ_7JdwSvW6fWs6NkZzYvxBW";
    //}
    //this.codePush = new CodePush(deploymentKey, this, BuildConfig.DEBUG);
    return deploymentKey;
  }

  @Nullable
  @Override
  protected String getJSBundleFile() {
    //return codePush.getBundleUrl("index.android.bundle");
    return CodePush.getBundleUrl();
  }



  @Override protected boolean getUseDeveloperSupport() {
    return BuildConfig.DEBUG;
  }
  @Override
  protected List<ReactPackage> getPackages() {
    //getKey();
    return Arrays.<ReactPackage>asList(
        new MainReactPackage()
        ,new RNDeviceInfo()
        //,this.codePush
        ,new CodePush(getKey(), this, BuildConfig.DEBUG)
        ,new WeChatPackage()
        //,new IntentReactPackage()
        //,new ToastReactPackage()
    );
  }

}