/**
 * 多数情况下，type 会被定义成字符串常量。当应用规模越来越大时，建议使用单独的模块或文件来存放 action。
 * 样板文件使用提醒:
 * 对于小应用来说，使用字符串做 action type 更方便些。不过，在大型应用中把它们显式地定义成常量还是利大于弊的。
 *
 * @type {string}
 */
/**
 * 文章
 * @type {string}
 */
export const FETCH_ARTICLE_LIST = 'FETCH_ARTICLE_LIST';
export const RECEIVE_ARTICLE_LIST = 'RECEIVE_ARTICLE_LIST';

/**
 * 文章类型
 * @type {string}
 */
export const FETCH_TYPE_LIST = 'FETCH_TYPE_LIST';
export const RECEIVE_TYPE_LIST = 'RECEIVE_TYPE_LIST';


export const RECEIVE_ARTICLE_LIST_MORE = 'RECEIVE_ARTICLE_LIST_MORE';

/**
 * 福利
 * @type {string}
 */
export const RECEIVE_WELFARE_LIST = 'RECEIVE_WELFARE_LIST';