'use strict';

import React from 'react';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';

import App from './containers/App';

const store = configureStore();
/**
 * 注入 Redux Store
 * 在应用中，只有最顶层组件是对 Redux 可知（例如路由处理）这是很好的。所有它们的子组件都应该是“笨拙”的，并且是通过 props 获取数据。
 *              容器组件 	             展示组件
 * 位置 	    最顶层，路由处理 	        中间和子组件
 * 使用      Redux 	是 	否
 * 读取数据 	从 Redux 获取 state 	    从 props 获取数据
 * 修改数据 	向 Redux 发起 actions 	从 props 调用回调函数
 */
class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

export default Root;

// AppRegistry.registerComponent('reading', () => Root);
// ReactRom.render(
// render(
//     <Provider store={store}>
//         <App />
//     </Provider>,
//     document.getElementById('root')
// )