const React = require('react-native');
const Colors = require('./Colors');
const CommonStyles = require('./CommonStyles');
const Platform = require('Platform');
const ErrorPlacehoderComponent = require('./ErrorPlacehoderComponent');

const {
    View,
    ActivityIndicatorIOS,
    ProgressBarAndroid,
} = React;


class CommonComponents {
    /**
     * 加载loading
     * @returns {XML}
     */
    static renderLoadingView() {
        if (Platform.OS === 'android') {
            return (
                <View style={CommonStyles.container}>
                    <ProgressBarAndroid styleAttr="Inverse"/>
                </View>
            )
        } else if (Platform.OS === 'ios') {
            return (
                <View style={CommonStyles.container}>
                    <ActivityIndicatorIOS size="large"/>
                </View>
            );
        }
    }

    /**
     * 错误加载
     * @param title
     * @param desc
     * @param onPress
     * @returns {XML}
     */
    static errorPlaceholder(title,desc,onPress) {
        return (
            <ErrorPlacehoderComponent title={title} desc={desc} onPress={onPress}/>
        )
    }

    /**
     * 公共的线样式
     * @returns {XML}
     */
    static renderSepLine() {
        return (
            <View style={CommonStyles.sepLine}/>
        )
    }
}

module.exports = CommonComponents;
