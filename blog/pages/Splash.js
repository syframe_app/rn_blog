'use strict';

import React from 'react';
import {
    Dimensions,
    Image,
    InteractionManager
} from 'react-native';

import MainContainer from '../containers/home/MainContainer';
// import AboutContainer from '../containers/setting/AboutContainer';

var {height, width} = Dimensions.get('window');

class Splash extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //可以把这个放到构造中去
        const {navigator} = this.props;

        setTimeout(() => {
            //Interactionmanager可以将一些耗时较长的工作安排到所有互动或动画完成之后再进行。这样可以保证JavaScript动画的流畅运行。
            // runAfterInteractions(): 在稍后执行代码，不会延迟当前进行的动画。
            InteractionManager.runAfterInteractions(() => {
                navigator.resetTo({//跳转到新的场景，并且重置整个路由栈
                    component: MainContainer,
                    name: 'Main'
                    // component: AboutContainer,
                    // name: 'About'
                });
            });
        }, 2000);
    }

    render() {
        return (
            <Image
                style={{flex: 1, width: width, height: height}}
                source={require('../../assets/imgs/splash.png')}
            />
        );
    }
}

export default Splash;