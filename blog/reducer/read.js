'use strict';

import * as types from '../constants/ActionTypes'

const initialState = {
	isRefreshing: false,
	loading: false,
	isLoadMore: false,
	noMore: false,
	articleList: {}
};

/**
 * 数据层
 */
export default function read(state = initialState, action) {
// export default function read(state = [], action) {
	// 上面方式:一个技巧是使用 ES6 参数默认值语法 来精简代码。
	// if (typeof state === 'undefined') {
	// 	return initialState
	// }

	// 1:如果 state 是普通对象，永远不要修改它！
	// 比如，reducer 里不要使用 Object.assign(state, newData)，应该使用 Object.assign({}, state, newData)。这样才不会覆盖旧的 state。
	// 2: 也可以使用 Babel 阶段 1 中的 ES7 对象的 spread 操作 特性中的 return { ...state, ...newData }。
	switch (action.type) {
		case types.FETCH_ARTICLE_LIST://需要做的只是改变 state 中的 visibilityFilter
			//不直接修改 state 中的字段，而是返回新对象。新的 todos 对象就相当于旧的 todos ,todos:{}在末尾加上新建的 对象。
			// 而这个新的 对象 又是基于 action 中的数据创建的
			// 尽管这样可行, 但 Object.assign() 冗长的写法会迅速降低 reducer 的可读性。
// 一个可行的替代方案是使用ES7的对象展开语法提案。该提案让你可以通过展开运算符 (...) ,
// 以更加简洁的形式将一个对象的可枚举属性拷贝至另一个对象。对象展开运算符在概念上与ES6的数组展开运算符相似。 我们试着用这种方式简化 todoApp :
			return Object.assign({}, state, {
				isRefreshing: action.isRefreshing,
				loading: action.loading,
				isLoadMore: action.isLoadMore
			});
/*			return [
				...state,
				{
					isRefreshing: action.isRefreshing,
					loading: action.loading,
					isLoadMore: action.isLoadMore
				}
			]*/
		case types.RECEIVE_ARTICLE_LIST:
			return Object.assign({}, state, {
				isRefreshing: false,
				isLoadMore: false,
				noMore: action.articleList.length == 0,
				articleList: state.isLoadMore ? loadMore(state, action) : combine(state, action),
				loading: state.articleList[action.typeId] == undefined
			});
		// 当 store 创建后，Redux 会 dispatch 一个 action 到 reducer 上，来用初始的 state 来填充 store。
		// 你不需要处理这个 action。但要记住，如果第一个参数也就是传入的 state 如果是 undefined 的话，reducer 应该返回初始的 state 值。
		default:
			return state
	}
}/**
 需要修改数组中指定的数据项而又不希望导致突变, 因此我们的做法是在创建一个新的数组后, 将那些无需修改的项原封不动移入, 接着对需修改的项用新生成的对象替换。
 case TOGGLE_TODO:
 return state.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      })
 //
 case ADD_TODO:
 return [
 ...state,
 {
     text: action.text,
     completed: false
 }
 ]
 */

function loadMore(state, action) {
	state.articleList[action.typeId] = state.articleList[action.typeId].concat(action.articleList);
	return state.articleList;
}

function combine(state, action) {
	state.articleList[action.typeId] = action.articleList;
	return state.articleList;
}

